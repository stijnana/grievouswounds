<?php

function load_stylesheets(){
    wp_register_style('bootstrap', get_template_directory_uri(). '/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('style', get_template_directory_uri(). '/style.css', array(), false, 'all');
    wp_enqueue_style('style');

    wp_register_style('stylesass', get_template_directory_uri(). '/css/style.css', array(), false, 'all');
    wp_enqueue_style('stylesass');
}

add_action('wp_enqueue_scripts', 'load_stylesheets');

function load_jquery(){
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri(). '/js/jquery.js', '', true);
}

add_action('wp_enqueue_scripts', 'load_jquery');

function load_js(){
    wp_register_script('costumjs', get_template_directory_uri(). '/js/script.js', array(), 1, true);
    wp_enqueue_script('costumjs');

    wp_register_script('boostrapjs', get_template_directory_uri(). '/js/bootstrap.js', array(), 1, true);
    wp_enqueue_script('boostrapjs');

    wp_register_script('paralax', get_template_directory_uri(). '/js/parallax/parallax.js', array(), 1, true);
    wp_enqueue_script('paralax');
}

add_action('wp_enqueue_scripts','load_js');

add_theme_support('menus');

register_nav_menus(
    array(
        'top-menu' => __('Top-Menu', 'theme'),
        'footer-menu' => __('Footer-Menu', 'theme')
    )
);