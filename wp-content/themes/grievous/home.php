<?php
get_header(); ?>


<?php

// check if the flexible content field has rows of data
if (have_rows('page_builder')):
    ?>
    <div class="home-wrapper">
        <?php
        // loop through the rows of data
        while (have_rows('page_builder')) : the_row();

            //break img block
            if (get_row_layout() == 'imgs'):
                get_sub_field('break_img'); ?>
                <div class="parallax-window" data-parallax="scroll"
                     data-image-src="<?php the_sub_field('break_img'); ?>">
                </div>
            <?php

            //simpel text block
            elseif (get_row_layout() == 'text_field'):
                ?>
                <div class="textblock">
                    <div class="row content-container">
                        <div class="col">
                            <h1><?php the_sub_field('title') ?></h1>
                        </div>
                        <div class="col">
                            <p><?php the_sub_field('text_field') ?></p>\
                        </div>
                    </div>
                </div>
            <?php
            endif;

        endwhile;
        ?>
    </div>
<?php
endif;

?>


<?php get_footer() ?>